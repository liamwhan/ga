﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetic1
{
    class Generation
    {
        public List<Chromosome> LastGeneration = new List<Chromosome>();
        public List<Chromosome> CurrentGeneration = new List<Chromosome>();
        public int GenerationCount = 1;
        public int MaxFitness = 0;
        public int SinceIncrease = 0;
        public int FitnessTarget = 10;
        public readonly int MaxGenerations = 100;
        private int PopulationSize;
        private Random RandomGen = new Random();
        private double MutationProbabilty = 0.5;


        private string logFilePath = Path.Combine(Directory.GetCurrentDirectory(), "Output.log");

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="populationSize">Optionally specify the population size</param>
        public Generation(int populationSize = 10)
        {
            if(populationSize % 2 != 0)
            {
                throw new Exception("The population size must be an even integer... despite what you may have heard to the contrary, the chicken did NOT come before the egg.");
            }

            

            PopulationSize = populationSize;

            InitPopulation();

           


            //At the beginning of the Process and after Attrition (blending of the Generations into a new Fittest Members generation 
            LastGeneration.AddRange(CurrentGeneration);
            WriteGenerationToLog();

            while (CheckFitness())
            {
                NextGen();
                WriteGenerationToLog("*** Before Attrition ***", "*** END Before Attrition ***");
                Attrition();
                WriteGenerationToLog("*** After Attrition ***", "*** END After Attrition ***");
                LogGeneration(CurrentGeneration);
            }

            Exit();
            
        }

        private void WriteToLog(string message, bool first = false)
        {
            string[] messageList = { message + Environment.NewLine };
            if(first)
            {
                File.WriteAllLines(logFilePath, messageList);
            } else
            {
                File.AppendAllLines(logFilePath, messageList);
            }
            
        }

        private void WriteToLog(IEnumerable<string> lines)
        {
            File.AppendAllLines(logFilePath, lines);
        }


        private void WriteGenerationToLog(string prefix = null, string suffix = null)
        {
            List<string> Genome = new List<string>();

            if (prefix != null)
            {
                Genome.Add(prefix);    
            }
            
            
            Genome.Add("--- Generation " + GenerationCount.ToString() + " ---");


            foreach (Chromosome chrome in CurrentGeneration)
            {
                
                Genome.Add(chrome.ChromeCode);
            }

            Genome.Add("---/ END Generation " + GenerationCount.ToString() + " /---");
            
            if(suffix != null)
            {
                Genome.Add(suffix);
            }

            WriteToLog(Genome);

            
        }

        //Step 1 - produce an initial population of individuals
        private void InitPopulation()
        {
            int i = 0;

            while (i <= PopulationSize)
            {
                CurrentGeneration.Add(new Chromosome(RandomGen, 10));
                i++;

            }

            GenerationCount++;
        }

        private void Exit()
        {
            Console.WriteLine("Evolution Complete.");
            Console.WriteLine(GenerationCount.ToString() + " generations were produced.");
            Console.WriteLine("Highest Fitness Score: " + MaxFitness);
            WriteToLog(Environment.NewLine + "EVOLUTION COMPLETE");
            WriteToLog(GenerationCount.ToString() + " generations were produced.");
            WriteToLog("Highest Fitness Score: " + MaxFitness);

            Process.Start(new ProcessStartInfo("notepad.exe", logFilePath));


        }

        public void LogGeneration(List<Chromosome> generation)
        {
            Console.WriteLine("--- Generation " + GenerationCount.ToString() + " ---");
            int index = 0;
            foreach (Chromosome chrome in generation)
            {
                index++;
                LogIndividual(chrome, index);
            }
            Console.WriteLine("---/ END Generation " + GenerationCount.ToString() + " /---");
        }

        private void LogIndividual(Chromosome chrome, int index)
        {
            Console.WriteLine("--- Individual " + index.ToString() + " ---");
            Console.WriteLine("Name: " + chrome.Name);
            Console.WriteLine("Parents: " + string.Join(", ", chrome.Parents.ToArray()));
            Console.WriteLine("Chromosome: " + chrome.ChromeCode);
            Console.WriteLine("Is Mutant: " + chrome.IsMutant.ToString());
            Console.WriteLine("---/ END Individual " + index.ToString() + " /---");
        }

        public void NextGen()
        {
            
            Console.WriteLine("--- Starting New Generation (" + Extensions.AddOrdinal(GenerationCount) + ") ---");

            //Loop through each Couple
            int lastGenerationCount = LastGeneration.Count; 

            for (int i = 0; i < lastGenerationCount; i += 2)
            {
                //Random offspring count per couple
                int litterCount = RandomGen.Next(1, 11);

                for (int j = 0; j < litterCount; j++)
                {
                    if(i + 1 <= LastGeneration.Count - 1)
                    {
                        CurrentGeneration.Add(Mate(LastGeneration[i], LastGeneration[i + 1]));
                    } else
                    {
                        return;
                    }
                    
                }
            }

            GenerationCount++;
            Console.WriteLine("--- / New Generation Created /---");

        }

        public Chromosome Mate(Chromosome mum, Chromosome dad)
        {

            //How many children (Random litter size between 1 and 10)

            List<Chromosome> children = new List<Chromosome>();


            //Where in the chromosome are we splicing from
            //Note: Pig and Elephant DNA just won't splice - Google Loverboy Pig and Elephant DNA for details
            int pivot = RandomGen.Next(0, PopulationSize);
            int chromeLength = mum.GeneList.Count;

            //Get Genome from Mother up to pivot, get rest from father from pivot

            List<int> childCode = new List<int>();
            childCode.AddRange(mum.GeneList.GetRange(0, pivot));
            childCode.AddRange(dad.GeneList.GetRange(pivot, (dad.GeneList.Count) - pivot));

            Chromosome child;
            


            double mutationChance = RandomGen.NextDouble();
            if (MutationProbabilty > mutationChance)
            {
                //Mutate - same code at pivot but we dont necessarily want the same point in the gene.
                int mutantLocus = RandomGen.Next(0, chromeLength);
                int mutantGene = 1 - childCode[mutantLocus];

                childCode[mutantLocus] = mutantGene;

                child = new Chromosome(RandomGen, childCode);
                child.IsMutant = true;
            }
            else
            {
                child = new Chromosome(RandomGen, childCode);
            }

            child.Parents.Add(mum.Name);
            child.Parents.Add(dad.Name);

            Console.WriteLine("Child Created...");

            return child;



        }

        public void Attrition()
        {


            //Sort the Entire Population and CurrentGeneration by Fitness
            LastGeneration.Sort((x, y) => y.Fitness.CompareTo(x.Fitness));
            CurrentGeneration.Sort((x, y) => y.Fitness.CompareTo(x.Fitness));

            //Get the pivot point of the List (half the collection)
            int pivot = 5;

            List<Chromosome> FittestGen = new List<Chromosome>();

            FittestGen.AddRange(LastGeneration.Take(pivot));
            FittestGen.AddRange(CurrentGeneration.Take(pivot));

            //Now resort the new List
            FittestGen.Sort((x, y) => y.Fitness.CompareTo(x.Fitness));

            LastGeneration = FittestGen;
            CurrentGeneration = FittestGen;

            LogGeneration(CurrentGeneration);

        }

        private bool CheckFitness()
        {
            int currentGenFitness = CurrentGeneration.Max(c => c.Fitness);

            //Has the target Fitness been Reached? 
            if(currentGenFitness >= FitnessTarget)
            {
                return false;
            }

            if (currentGenFitness > MaxFitness)
            {
                MaxFitness = CurrentGeneration[0].Fitness;
                SinceIncrease = 0;
           
            } else
            {
                SinceIncrease++;
            }

            return true;

        }
    }
}
