﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetic1
{

    
    public static class NameGenerator
    {
        private static string names = "Ayesha,Kiesha,Lauren,Violeta,Norberto,Birgit,Camelia,Hannelore,Laurette,Janell,Carolyne,Jovan,Denis,Genesis,Eliz,Tobie,Chantel,Tonja,Dillon,Lanita,Lorenza,Keely,Gertha,Vada,Eusebia,Brigida,Sherill,Sergio,Myrta,Mickey,Carma,Jamie,Beaulah,Virgina,Laraine,Willia,Shakira,Latricia,Mika,Dewitt,Herb,Rachelle,Twanna,Paulita,Kyoko,Danae,Tatyana,Della,Lucien,Adriane";

        public static List<string> Names = new List<string>(names.Split(','));

        public static string GetRandomName(Random rnd)
        {
            int nameIndex = rnd.Next(0, Names.Count);

            return Names.ElementAt(nameIndex);
        }
    }
}
