﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetic1
{

    
    class Program
    {
       
        static void Main(string[] args)
        {
            /*
            High level Algorithm
            
            while (termination condition not me) do
                select fitter individuals for reproduction
                recombine between individuals
                mutate individuals
                evaluate fitness of the modified individuals
                generate a new population
            end while


            GA Components
            Population
            Selection
            Mutation
            Recombination

            Example: MAXONE problem
            Maximise the number of 1 digits in binary string.
            
            Each 1 represents a correct answer to a question, while 0 represents an incorrect answer
            so we are maximizing the number of correct answers.

            The number of questions (and length of the string) is represented by L

            e.g. If L=10 then, 1 = 0000000001 (10 bits with 1 correct answer)

            

            */

            
            Generation evolver = new Generation();


            


        }

        
    }
}
