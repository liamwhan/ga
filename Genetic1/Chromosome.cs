﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetic1
{
    class Chromosome
    {
        private List<int> _genes;

        private Random _random;

        public List<int> GeneList
        {
            get
            {
                if(_genes != null)
                {
                    return _genes;
                } else
                {
                    throw new Exception("Gene Code is not defined.");
                }
            }
        }


        public string Name;
        public List<string> Parents = new List<string>();
        public bool IsMutant = false;
        public int GeneCount
        {
            get
            {
                return _genes.Count;
            }
        }

        public int Fitness
        {
            //Step 2. Evaluate the Fitness of the Chromosome
            get
            {
                return _genes.Sum();
            }
        }

        public string ChromeCode
        {
            get
            {
                return string.Join(",", this._genes.ToArray());
            }
        }

        public int GetGene(int index)
        {
            return _genes.ToArray()[index];
        }
 /// <summary>
 /// Base Constructor
 /// </summary>
 /// <param name="rnd"></param>
        private Chromosome(Random rnd)
        {
            _random = rnd;
            Name = NameGenerator.GetRandomName(rnd);
        }


 /// <summary>
 /// Constructor - Initalise Chromosome with an integer array
 /// </summary>
 /// <param name="rnd"></param>
 /// <param name="genes"></param>
        public Chromosome(Random rnd, int[] genes)
            :this(rnd)
        {
            _genes = new List<int>(genes);

        }

/// <summary>
/// Constructor - Random Gene Generation
/// </summary>
/// <param name="rnd">The Random Generator - needs to be passed as an argument so we dont run into identical clock time issues</param>
/// <param name="chromosomeLength">The bit length of the Chromosome</param>
        public Chromosome(Random rnd, int chromosomeLength)
            :this(rnd)
        {
            List<int> genes = new List<int>();
            



            for (int i = 0; i < chromosomeLength; i++)
            {

                genes.Add(_random.Next(0, 2));
            }

            _genes = genes;

            

        }

        /// <summary>
        /// Constructor - Initialize with a binary string
        /// </summary>
        /// <param name="rnd"></param>
        /// <param name="genomeString"></param>
        public Chromosome(Random rnd, string genomeString)
            :this(rnd)
        {
            List<int> genes = new List<int>();
             
            char[] genome = genomeString.ToCharArray();
            
            foreach(char gene in genome)
            {
                genes.Add((int)Char.GetNumericValue(gene));
            }

            _genes = genes;

        }

        /// <summary>
        /// Constructor - Initialize with integer List
        /// </summary>
        /// <param name="rnd"></param>
        /// <param name="genome"></param>
        public Chromosome(Random rnd, List<int> genome)
            :this(rnd)
        {
            _genes = genome;
        }
    }

}
